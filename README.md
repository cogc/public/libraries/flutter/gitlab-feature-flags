
The `FeatureFlagProvider` is a Flutter/Dart class designed to help manage and provide feature flags in your application, allowing you to control feature availability based on the specified environment scope. This README will guide you through the usage of this class.

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Getting Started](#getting-started)
- [Usage](#usage)
  - [Initialization](#initialization)
  - [Supported strategies:](#supported-strategies)
  - [Checking Feature Availability](#checking-feature-availability)
  - [Serialization to JSON](#serialization-to-json)
  - [Testing Library](#testing-library)

## Getting Started

To get started, you need to include this class in your Flutter/Dart project. You can do this by adding the following import statement to your Dart code:

```dart
import 'package:feature_flags/feature_flags.dart';
```
Ensure that you have the required dependencies in your pubspec.yaml file:

```yaml
dependencies:
    flutter:
        sdk: flutter
    feature_flags:
        git:
            url: https://gitlab.com/cogc/public/libraries/flutter/gitlab-feature-flags.git

  # Include other dependencies as needed
```

## Usage
The FeatureFlagProvider class offers the following features:

### Initialization
To initialize the FeatureFlagProvider, you need to provide the environmentScope and call the initialize method with the API endpoint URL and other parameters. Here's how to do it:

```dart
    FeatureFlagProvider featureFlagProvider = FeatureFlagProvider(environmentScope: 'production');

    try {
        const String projectId = String.fromEnvironment('PROJECT_ID');
        const String featureFlagAccessToken = String.fromEnvironment('FEATURE_FLAG_ACCESS_TOKEN');
        
        await featureFlagProvider.initialize(
            url: Uri.parse('https://gitlab.com/api/v4/projects/$projectId/feature_flags'),
            privateToken: featureFlagAccessToken,
            bypassEnvironment: 'local', // Optional
        );
    } catch (ex) {
        print('Initialization error: $ex');
    }
```

The `bypassEnvironment` variable can be used to bypass the feature check. If the bypass environment matches the build environement, all features will be shown. This is to assist with development without having to trigger feature flags.

### Supported strategies:

1. All Users Strategy:

- Meaning: When you use the "All Users" strategy for a feature flag, it means the feature is accessible to all users, regardless of any specific user list or conditions.
- Use Case: You might use the "All Users" strategy when you want to enable a feature for all users, such as after testing and development are complete, and you're ready to release the feature to your entire user base.
- Configuration: This strategy doesn't involve specifying individual users or user lists. It's a straightforward method to make the feature available to everyone.

2. User List Strategy:

- Meaning: The "User List" strategy allows you to specify a list of users who have access to a particular feature. Only users on this list will be able to access the feature.
- Use Case: You would typically use the "User List" strategy when you want to control feature access for specific users, such as beta testers, premium customers, or a particular group of users. It's particularly useful for rolling out features to a limited audience for testing or gradual deployment.
- Configuration: You need to provide a list of specific users or user identifiers (e.g., email addresses, usernames) who should have access to the feature. Only those users will see the feature; others won't.

### Checking Feature Availability
You can check if a specific feature is available for a user within the configured environment scope using the isFeatureAvailable method. It considers the feature's activation state and any user-specific strategies:

```dart
String userList = 'user_list';
String username = 'user123'; // Optional

// If a username is supplied, it will check if the "User List" strategy is implemented for the feature
if (featureFlagProvider.isFeatureAvailable(userList, username)) {
  // Feature is available for the user.
} else {
  // Feature is not available for the user.
}

String allUsers = 'all_users';

// Default check to see if "All Users" strategy is implemented for the feature
if (featureFlagProvider.isFeatureAvailable(allUsers)) {
  // Feature is available for the user.
} else {
  // Feature is not available for the user.
}

```
### Serialization to JSON
You can serialize the list of feature flags to a JSON-serializable map using the featureFlagsToJson method:

```dart
Map<String, dynamic> flagsJson = featureFlagProvider.featureFlagsToJson();
```
This can be useful if you want to save the feature flags to a file or send them to a remote configuration service.

That's it! You now have a powerful feature flag management tool for your Flutter application. Experiment with different scopes and strategies to control feature availability based on your specific needs.

For more information on how to use this class, you can refer to the inline documentation in the source code.

### Testing Library

Replace the `gitlab_private_token`, `environment` and `project_id` with the appropriate values

```bash
flutter test --dart-define PRIVATE_TOKEN=gitlab_private_token --dart-define ENVIRONMENT=environment --dart-define PROJECT_ID=project_id
```