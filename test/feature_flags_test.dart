import 'package:feature_flags/models/feature_flag.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:feature_flags/feature_flags.dart';

void main() async {
  const String projectId = String.fromEnvironment('PROJECT_ID');
  const String privateToken = String.fromEnvironment('PRIVATE_TOKEN');
  String environment = 'test';

  const String userOne = 'test_one@goldcoast.qld.gov.au';
  const String userTwo = 'test_two@goldcoast.qld.gov.au';
  const String userThree = 'test_three@goldcoast.qld.gov.au';
  const String userFour = 'test_four@goldcoast.qld.gov.au';

  Uri url = Uri.parse(
    "https://gitlab.com/api/v4/projects/$projectId/feature_flags",
  );
  FeatureFlagProvider featureFlagProvider = FeatureFlagProvider(
    environmentScope: environment,
  );

  await featureFlagProvider.initialize(
    url: url,
    privateToken: privateToken,
  );

  test('Check API response', () async {
    expect(featureFlagProvider.statusCode, 200);
  });

  test('Check all users strategy', () async {
    FeatureFlag feature = featureFlagProvider.featureFlags
        .firstWhere((feature) => feature.name == 'test-all-users');

    expect(feature.active, true);
    expect(feature.strategies.length, 1);
    expect(feature.strategies[0].scopes.length, 3);
    expect(feature.strategies[0].userList, null);
  });

  test('Check user list strategy', () async {
    FeatureFlag feature = featureFlagProvider.featureFlags
        .firstWhere((feature) => feature.name == 'test-user-list');

    expect(feature.active, true);
    expect(feature.strategies.length, 2);
    expect(feature.strategies[0].scopes.length, 3);
    expect(feature.strategies[1].scopes.length, 2);
    expect(feature.strategies[0].userList!.userXids.length, 2);
    expect(feature.strategies[1].userList!.userXids.length, 2);
    expect(feature.strategies[0].userList!.name, 'testers');
    expect(feature.strategies[1].userList!.name, 'developers');
  });

  test('Default functionality', () async {
    bool available = featureFlagProvider.isFeatureAvailable('test-all-users');

    expect(available, true);
  });

  test('Check for non existant feature', () async {
    bool available = featureFlagProvider.isFeatureAvailable(
      'does-not-exist',
      username: userOne,
    );

    expect(available, false);
  });

  test('User list functionality', () async {
    String feature = 'test-user-list';
    bool one =
        featureFlagProvider.isFeatureAvailable(feature, username: userOne);
    bool two =
        featureFlagProvider.isFeatureAvailable(feature, username: userTwo);
    bool three =
        featureFlagProvider.isFeatureAvailable(feature, username: userThree);
    bool four =
        featureFlagProvider.isFeatureAvailable(feature, username: userFour);

    if (environment == 'develop') {
      expect(one, true);
      expect(two, true);
      expect(three, true);
      expect(four, true);
    }

    if (environment == 'test') {
      expect(one, true);
      expect(two, true);
      expect(three, true);
      expect(four, true);
    }

    if (environment == 'production') {
      expect(one, false);
      expect(two, false);
      expect(three, true);
      expect(four, true);
    }
  });

  test('Bypass environment check', () async {
    FeatureFlagProvider featureFlagProvider = FeatureFlagProvider(
      environmentScope: 'local',
    );
    featureFlagProvider.bypassEnvironment = 'local';
    bool bypassOne = featureFlagProvider.isFeatureAvailable(
      'local-check',
      username: userOne,
    );
    expect(bypassOne, true);

    featureFlagProvider.bypassEnvironment = 'test';
    bool bypassTwo = featureFlagProvider.isFeatureAvailable(
      'local-check',
      username: userOne,
    );
    expect(bypassTwo, false);
  });
}
