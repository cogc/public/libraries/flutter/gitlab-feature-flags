library feature_flags;

import 'dart:convert';
import 'package:feature_flags/models/feature_flag.dart';
import 'package:feature_flags/models/strategy.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

/// A class responsible for managing and providing feature flags based on the
/// specified environment scope. It allows initialization from an API endpoint,
/// retrieval of feature availability, and serialization to JSON.
class FeatureFlagProvider extends ChangeNotifier {
  List<FeatureFlag> featureFlags = [];
  int? statusCode;
  String environmentScope;
  String? bypassEnvironment;

  /// Creates a new [FeatureFlagProvider] instance with the specified
  /// [environmentScope].
  FeatureFlagProvider({required this.environmentScope});

  /// Initializes the feature flags by making an API request to the given [url]
  /// and populates the [featureFlags] list based on the provided environment
  /// scope. If a [bypassEnvironment] is specified, it will be considered as a
  /// special case.
  Future<void> initialize({
    required Uri url,
    required String privateToken,
    String? bypassEnvironment,
  }) async {
    this.bypassEnvironment = bypassEnvironment;
    try {
      var resp = await http.get(
        url,
        headers: {"PRIVATE-TOKEN": privateToken},
      );
      statusCode = resp.statusCode;
      if (statusCode == 200) {
        final List<dynamic> jsonList = json.decode(resp.body);

        featureFlags = jsonList
            .map((feature) => FeatureFlag.fromJson(feature))
            .where((flag) => flag.hasMatchingScope(environmentScope))
            .toList();
      } else {
        throw Exception('API request error: ${resp.statusCode}');
      }
      notifyListeners();
    } catch (ex) {
      throw Exception('Initialization error $ex');
    }
  }

  /// Checks if the specified [featureName] is available for the given [username]
  /// within the configured environment scope, taking into account any potential
  /// bypass environment.
  bool isFeatureAvailable(String featureName, {String? username}) {
    FeatureFlag? featureFlag = _getFeatureFlag(featureName);

    if (environmentScope == bypassEnvironment) {
      return true;
    }

    if (featureFlag != null) {
      bool isActive = featureFlag.active;

      bool isEnabledForUser = _enabledForUser(featureFlag, username);

      if (isActive && isEnabledForUser) {
        return true;
      }
    }

    return false;
  }

  /// Gets the description of the specified [featureName]
  String? featureFlagDescription(String featureName) {
    FeatureFlag? featureFlag = _getFeatureFlag(featureName);
    if (featureFlag != null) {
      return featureFlag.description;
    }
    return null;
  }

  /// Retrieves a feature flag by its name from the list of [featureFlags].
  FeatureFlag? _getFeatureFlag(String featureName) {
    FeatureFlag? flag;
    for (FeatureFlag feature in featureFlags) {
      if (feature.name == featureName) {
        flag = feature;
      }
    }
    return flag;
  }

  /// Determines if a feature flag is enabled for a user based on the feature's
  /// strategies and the user's [username].
  bool _enabledForUser(FeatureFlag featureFlag, String? username) {
    bool enabled = false;
    List<String> strategyNames = featureFlag.getStrategyNames();

    for (Strategy strategy in featureFlag.strategies) {
      if (_enabledForScope(strategy)) {
        if (strategyNames.any((name) => name == 'default')) {
          enabled = true;
        }

        if (strategyNames.any((name) => name == 'gitlabUserList') &&
            username != null) {
          if (strategy.userList!.userXids.any((user) => user == username)) {
            enabled = true;
          }
        }
      }
    }
    return enabled;
  }

  /// Checks if a specific [strategy] is enabled for the configured environment scope.
  bool _enabledForScope(Strategy strategy) {
    bool available = false;
    if (strategy.scopes
        .any((scope) => scope.environmentScope == environmentScope)) {
      return true;
    }
    return available;
  }

  /// Serializes the list of [featureFlags] to a JSON-serializable map.
  Map<String, dynamic> featureFlagsToJson() {
    return {
      'featureFlags': featureFlags.map((flag) => flag.toJson()).toList(),
    };
  }
}
