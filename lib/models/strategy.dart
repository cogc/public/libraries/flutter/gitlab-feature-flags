import 'package:feature_flags/models/scope.dart';
import 'package:feature_flags/models/user_list.dart';

class Strategy {
  int id;
  String name;
  Map<String, dynamic> parameters;
  List<Scope> scopes;
  UserList? userList;

  Strategy({
    required this.id,
    required this.name,
    required this.parameters,
    required this.scopes,
    required this.userList,
  });

  factory Strategy.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic>? users = json['user_list'];

    return Strategy(
      id: json['id'],
      name: json['name'],
      parameters: json['parameters'],
      scopes: List<Scope>.from(
        json['scopes'].map((scope) => Scope.fromJson(scope)),
      ),
      userList: users != null ? UserList.fromJson(users) : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'parameters': parameters,
      'scopes': scopes.map((scope) => scope.toJson()).toList(),
      'user_list': userList != null ? userList!.toJson() : {},
    };
  }
}
