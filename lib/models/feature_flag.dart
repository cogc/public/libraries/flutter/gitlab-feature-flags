import 'package:feature_flags/models/strategy.dart';

class FeatureFlag {
  String name;
  String description;
  bool active;
  String version;
  DateTime createdAt;
  DateTime updatedAt;
  List<String> scopes;
  List<Strategy> strategies;

  FeatureFlag({
    required this.name,
    required this.description,
    required this.active,
    required this.version,
    required this.createdAt,
    required this.updatedAt,
    required this.scopes,
    required this.strategies,
  });

  bool hasMatchingScope(String environmentScope) {
    return strategies.any((strategy) => strategy.scopes
        .any((scope) => scope.environmentScope == environmentScope));
  }

  List<String> getStrategyNames() {
    return strategies.map((strat) => strat.name).toList();
  }

  factory FeatureFlag.fromJson(Map<String, dynamic> json) {
    List<Strategy> strats = List<Strategy>.from(
      json['strategies'].map(
        (strategy) => Strategy.fromJson(strategy),
      ),
    );

    return FeatureFlag(
      name: json['name'],
      description: json['description'],
      active: json['active'],
      version: json['version'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
      scopes: List<String>.from(json['scopes']),
      strategies: strats,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'description': description,
      'active': active,
      'version': version,
      'created_at': createdAt.toIso8601String(),
      'updated_at': updatedAt.toIso8601String(),
      'scopes': scopes,
      'strategies': strategies.map((strategy) => strategy.toJson()).toList(),
    };
  }
}
