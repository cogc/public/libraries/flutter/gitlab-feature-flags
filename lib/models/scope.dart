class Scope {
  int id;
  String environmentScope;

  Scope({
    required this.id,
    required this.environmentScope,
  });

  factory Scope.fromJson(Map<String, dynamic> json) {
    return Scope(
      id: json['id'],
      environmentScope: json['environment_scope'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'environment_scope': environmentScope,
    };
  }
}
