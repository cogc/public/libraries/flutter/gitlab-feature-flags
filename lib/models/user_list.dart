class UserList {
  int id;
  int iid;
  String name;
  List<String> userXids;

  UserList({
    required this.id,
    required this.iid,
    required this.name,
    required this.userXids,
  });

  factory UserList.fromJson(Map<String, dynamic> json) {
    String userIds = json['user_xids'] as String;
    if (userIds.isEmpty) {
      throw Exception("No users in user list");
    }

    return UserList(
      id: json['id'],
      iid: json['iid'],
      name: json['name'],
      userXids: userIds.split(',').map((e) => e.toLowerCase()).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'iid': iid,
      'name': name,
      'userXids': userXids,
    };
  }
}
